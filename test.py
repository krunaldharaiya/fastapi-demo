import requests


print("Adding an Item :-")
print(
    requests.post(
        'http://127.0.0.1:8000/items/',
        json={"id": 3, "name": "Plywood", "price": 11.99, "count": 50, "category": "consumables"}
    ).json()
)
print("Getting List Of Items :-")
print(
    requests.get('http://127.0.0.1:8000/items/').json()
)
