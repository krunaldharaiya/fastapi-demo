from enum import Enum

from fastapi import FastAPI, HTTPException, Path, Query
from pydantic import BaseModel

app = FastAPI()


class Category(Enum):
    TOOLS = "tools"
    CONSUMABLES = "consumables"


class Item(BaseModel):
    id: int
    name: str
    price: float
    count: int
    category: Category


items = {
    0: Item(id=0, name="Hammer", price=9.99, count=20, category=Category.TOOLS),
    1: Item(id=1, name="Pliers", price=5.99, count=20, category=Category.TOOLS),
    2: Item(id=2, name="Nails", price=1.99, count=100, category=Category.CONSUMABLES),
}


# APIs
@app.get("/")
async def root() -> dict[str, str]:
    return {"message": "welcome to the world with fast API!"}


# APIs
@app.get("/items/")
async def list_items() -> dict[str, dict[int, Item]]:
    return {"items": items}


@app.get("/items/{item_id}")
async def query_item_by_id(item_id: int) -> Item:
    if item_id not in items:
        raise HTTPException(status_code=404, detail=f"The item with {item_id=} does not exist.")
    return items[item_id]


selection_query = dict[str, str | int | float | Category | None ] # this contains the user's query arguments

@app.get("/items/")
async def query_item_by_params(
    name: str | None = None,
    price: float | None = None,
    count: int | None = None,
    category: Category | None = None
) -> dict[str, list[Item]]:
    def check_item(item: Item) -> bool:
        return all(
            (name is None or name == item.name or item.name in name,
            price is None or price == item.price,
            count is None or count == item.count,
            category is None or category is item.category)
        )
    
    data = [item for item in items.values() if check_item(item)]
    return {
        "data": data
    }


# create a item
@app.post("/items/")
async def create_item(item: Item) -> dict[str, Item]:
    if item.id in items:
        raise HTTPException(status_code=400, detail="Item already exists")
    
    items[item.id] = item
    return {
        "Added": item
    }


# Update an item
@app.put(
    "/items/{item_id}",
    responses={
        404: {"description": "Item not found"},
        400: {"description": "No arguments specified"},
    },
)
async def update_item(
    item_id: int = Path(
        title="Item ID", description="Unique integer that specifies an item.", ge=0
    ),
    name: str
    | None = Query(
        title="Name",
        description="New name of the item.",
        default=None,
        min_length=1,
        max_length=8,
    ),
    price: float
    | None = Query(
        title="Price",
        description="New price of the item in Euro.",
        default=None,
        gt=0.0,
    ),
    count: int
    | None = Query(
        title="Count",
        description="New amount of instances of this item in stock.",
        default=None,
        ge=0,
    ),
) -> dict[str, Item]:
    if item_id not in items:
        raise HTTPException(status_code=404, detail=f"Item {item_id=} does not exist.")
    if all(info is None for info in (name, price, count)):
        raise HTTPException(status_code=400, detail="No Parameter provided for update")
    
    item = items[item_id]
    if name is not None:
        item.name = name
    if price is not None:
        item.price = price
    if count is not None:
        item.count = count
    
    return {"updated": item}


# delete a item
@app.delete("/items/{item_id}")
async def delete_item(item_id:int) -> dict[str, Item]:
    if item_id not in items:
        raise HTTPException(status_code=404, detail=f"Item with {item_id=} does not exist.")

    item = items.pop(item_id)
    return {"deleted": item}
