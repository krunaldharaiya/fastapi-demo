# fastapi-demo



## Getting started

Installation and usage
----------------------

First, create virtual-env and install all requirement from requirement.txt ::

    $ python -m venv venv
    
Activate virtual-env then install requirements

    $ pip install -r requirements.txt

Now run server

    $ uvicorn main:app --reload